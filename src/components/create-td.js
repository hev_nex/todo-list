import React, { Component } from 'react';

export default class CreateTd extends Component {
	render() {
		return (
			<form onSubmit={this.handleCreate.bind(this)}>

			  <input 	ref="createInput" 	type='text' placeholder="Your task" size="30" required /><p />

			  <textarea ref="createDescribe" placeholder="Describe of the task" rows="4" cols="34" ></textarea><p />

			  <select 	ref="createSelect" size="1">
				  <option value="Common">			Common			</option>
				  <option value="Important">		Important		</option>
				  <option value="Very Important">	Very Important	</option>
			  </select><p />

			  <input 	ref="deadline" 	type='datetime-local' /><p />

			  <button className='create-td__button'>Create</button>

			</form>
		);
	}
	handleCreate(event) {
		event.preventDefault();

		const task = this.refs.createInput.value;
		const describe = this.refs.createDescribe.value;
		const importance = this.refs.createSelect.value;
		const deadline = this.refs.deadline.value;

		this.props.createTask(task, describe, importance, deadline);
		this.refs.createInput.value = '';
		this.refs.createDescribe.value = '';
	}

}
