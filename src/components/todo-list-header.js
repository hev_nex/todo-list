import React, { Component } from 'react';


export default class TodoListHeader extends Component {
  render() {
    return (
       <thead>
          <tr>
            <th>Task</th>
            <th>Describe</th>
            <th>Importance</th>
            <th>Deadline</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
       </thead>
    );
  }
}