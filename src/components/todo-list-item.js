import React, { Component } from 'react';


export default class TodoListItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isEditing: false
    };
  }

  renderTaskSection() {
    const { task, isCompleted} = this.props;
    const taskStyle = {
      textDecoration: isCompleted ? 'line-through' : 'none',    
    };
    if (this.state.isEditing) {
      return (
        <td>
          <form onSubmit={this.onSaveClick.bind(this)}>
            <input type="text" defaultValue={task} ref="editInput" />
          </form>
        </td>
      )
    }
    return (
      <td style={taskStyle} onClick={this.props.doneTask.bind(this, task)}>
        <span className="task">
          {task}
        </span>
      </td>
    );
  }

  renderActionsSection() {
    if (this.state.isEditing) {
      return (
        <td>
          <span className="list-item__button" onClick={this.onSaveClick.bind(this)}>Save</span>
          <span className="list-item__button" onClick={this.onCancelClick.bind(this)}>Cancel</span>
        </td>
      );
    }
    return (
      <td>
        <span className="list-item__button" onClick={this.onEditClick.bind(this)}>Edit </span>
        <span className="list-item__delete" onClick={this.props.deleteTask.bind(this, this.props.task)}>✖</span>
      </td>
    );
  }

  renderImportanceSection() {
    const { importance } = this.props;
    return (
      <td>
        {importance}
      </td>
    );
  }

  renderDescribeSection() {
    const { describe } = this.props;
    return (
      <td>
        {describe}
      </td>
    );
  }

  renderDedlineSection() {
    const { deadline } = this.props;
    return (
      <td>
        {deadline}
      </td>
    );
  }

  renderStatusSection() {
    const { status, isCompleted } = this.props;
    const statusStyle = {
      fontWeight: 600,
      color: isCompleted ? 'black' : (status === 'In process...') ? '#736262' : '#522A12'
    };
      return (
      <td style={statusStyle}>
        {status}
      </td>
      );
  }

  render() {
    return (
      <tr>
        {this.renderTaskSection()}
        {this.renderDescribeSection()}
        {this.renderImportanceSection()}
        {this.renderDedlineSection()}
        {this.renderStatusSection()}
        {this.renderActionsSection()}
      </tr>
    );
  }

  onEditClick() {
    this.setState({ isEditing: true });
  }

  onCancelClick() {
    this.setState({ isEditing: false });
  }

  onSaveClick(event) {
    event.preventDefault();

    const oldTask = this.props.task;
    const newTask = this.refs.editInput.value;
    this.props.saveTask(oldTask, newTask);
    this.setState({ isEditing: false });
  }
}