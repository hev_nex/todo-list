import _ from 'lodash';
import React from 'react';
import TodoList from './todo-list';
import CreateTd from './create-td';
import TodoFilter from './todo-filter';

var todo = [];
todo = JSON.parse(localStorage.todo);
const optionDate = {
  month: 'long',
  day: 'numeric',
  hour: 'numeric',
  minute: 'numeric',
};

const todoRefresh = _.map(todo, refreshStatus);

function refreshStatus(obj) {
  var dateNow = new Date();
  dateNow = dateNow.toLocaleString("en-US", optionDate);

  if (!obj.isCompleted) {
    if (obj.deadline < dateNow && obj.deadline !== '-') {
      obj.status = 'Time is over!';
      return obj;
    } else { return obj; }
  } else { return obj; }
}

todo = todoRefresh;
var str = JSON.stringify(todo);
localStorage.setItem("todo", str);

export default class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      todo
    };
  }

  render() {
    return (
      <div className='app'>
        <div className='app__createTd'>
          <CreateTd createTask={this.createTask.bind(this)} />
        </div>
        <div className='app__todoFilter'>
          <TodoFilter filterTodo={this.filterTodo.bind(this)} />
        </div>
        <TodoList
          todo={this.state.todo}
          doneTask={this.doneTask.bind(this)}
          saveTask={this.saveTask.bind(this)}
          deleteTask={this.deleteTask.bind(this)}
        />
      </div>
    );
  }

  doneTask(task) {
    const foundTd = _.find(this.state.todo, td => td.task === task);
    foundTd.isCompleted = !foundTd.isCompleted;

    var dateCompleted = new Date();
    dateCompleted = dateCompleted.toLocaleString("en-US", optionDate);

    if (foundTd.isCompleted) {
      foundTd.status = '✓ ' + dateCompleted;
    } else if (foundTd.deadline > dateCompleted || foundTd.deadline === '-') {
      foundTd.status = 'In process...';
    } else {
      foundTd.status = 'Time is over!';
    }
    this.storageTask();
    this.setState({ todo: this.state.todo });
  }

  createTask(task, describe, importance, deadline) {
    var date = new Date(deadline);
    if (date == "Invalid Date")
      date = '-';
    else 
      date = date.toLocaleString("en-US", optionDate);

    this.state.todo.push({
      task,
      describe,
      importance,
      deadline: date,
      status: 'In process...',
      isCompleted: false
    });
    this.storageTask();
    this.setState({ todo: this.state.todo });
  }

  saveTask(oldTask, newTask) {
    const foundTd = _.find(this.state.todo, td => td.task === oldTask);
    foundTd.task = newTask;
    this.storageTask();
    this.setState({ todo: this.state.todo });
  }

  deleteTask(taskToDelete) {
    _.remove(this.state.todo, td => td.task === taskToDelete);
    this.storageTask();
    this.setState({ todo: this.state.todo });
  }

  storageTask() {
    var str = JSON.stringify(todo);
    localStorage.setItem("todo", str);
    this.setState({ todo: this.state.todo });
  }

  filterTodo(option) {
    if (option !== 'all') {
      const filtered = _.filter(JSON.parse(localStorage.todo), ['importance', option]);
      this.setState({ todo: filtered });
    } else
      this.setState({ todo: JSON.parse(localStorage.todo) });
  }
}



