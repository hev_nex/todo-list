import React, { Component } from 'react';

export default class TodoFilter extends Component {

  constructor(props) {
    super(props);
    this.handleOptionChange = this.handleOptionChange.bind(this);
  }

  render() {
    return (
      <form onChange={this.handleOptionChange}>
        <select size="1">
          <option value="all">            All            </option>
          <option value="Common">         Common         </option>
          <option value="Important">      Important      </option>
          <option value="Very Important"> Very important </option>
        </select>
      </form>
    );
  }
  
  handleOptionChange(event) {
    var option = event.target.value;
    this.props.filterTodo(option);
  }
}